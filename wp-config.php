<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'myblog');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'L`en_/hw>|K{XLqL6yNy2Uv,r:RH6ycO6t;}On2~1vd{Sh?54Iwi)_B$pHy.*1xM');
define('SECURE_AUTH_KEY',  'Dc.kr%(7^TcH`9!G^$MSR_)&S2P9.1DP%c1g~$S=&5UF Wq9&A*CYt5Y$Ro|uo-8');
define('LOGGED_IN_KEY',    'Y{dwh).iB B>*|nP/vtSr>G1muSzq`x/LIM&0p|t]s5qE4NOg[aj1`Y^`YrH{/B:');
define('NONCE_KEY',        'm_KF<c0g]HaL7BFjIB,b8jwVn4:Xk6l(#hej&?lFEgY[08lI-3&QT3Mx{WVkux#a');
define('AUTH_SALT',        ',Es7VQEScyK4{l6WL1t7(#Du!Mu`msW^_-3Ma:v>$?*X/X2kn@{6le4y|0{W?h_R');
define('SECURE_AUTH_SALT', 'TZ6+)NE~ttkk3RbEu-bo:yiv?Xr$d80/S?||byUgb!wRnaZKeKUby@zH4rNw)Rv5');
define('LOGGED_IN_SALT',   'B3<siU)zd*T=w.23KEODnIlF&Q7BivT}1&<C}>3NLw@(gND9xt`Nk:(OOWipP+|*');
define('NONCE_SALT',       'BOB+vh{Okpji >VR*py~~.w>%Ev/sYop4$Vb5BqA$w@LQ:1Sh9ncs4+G3W-0`i03');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'blog_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
