			<?php get_header(); ?>
			<div id="content">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-9">
							<div class="post-news">
								<!-- Get post mặt định -->
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
	<?php setpostview(get_the_id()); ?>
	<h1 class="single-title"><?php the_title(); ?></h1>
	<div class="meta">
		<div class="info-content">
		<span>Ngày đăng: <?php echo get_the_date(); ?></span>
		<span>Lượt xem: <?php echo getpostviews(get_the_id()); ?></span>
	</div>
		<article class="post-content">
				<?php the_content(); ?>
		</article>
	</div>
<?php endwhile; else : ?>
<?php endif; ?>
<!-- Get post mặt định -->
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-3">
							<div class="sidebar">
							<?php get_sidebar(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php get_footer(); ?>
			